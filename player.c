#include "bot.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double myX,myY,myXp,myYp,randv;
int start;
int virusvar = 0;

int dist(const void *a,const void *b)
{
    double ax = ((struct food *)a) -> x;
    double ay = ((struct food *)a) -> y;
    double dista = sqrt((myX -ax)*(myX -ax) + (myY-ay)*(myY-ay) );
    
    double bx = ((struct food *)b) -> x;
    double by = ((struct food *)b) -> y;
    double distb = sqrt((myX -bx)*(myX -bx) + (myY-by)*(myY-by) );
    
    return dista - distb;
}
int distplayer(const void *a,const void *b)
{
    double ax1 = ((struct player *)a) -> x;
    double ay1 = ((struct player *)a) -> y;
    double distap = sqrt((myXp -ax1)*(myXp -ax1) + (myYp-ay1)*(myYp-ay1) );
    
    double bx1 = ((struct player *)b) -> x;
    double by1 = ((struct player *)b) -> y;
    double distbp = sqrt((myXp -bx1)*(myXp -bx1) + (myYp-by1)*(myYp-by1) );
    
    return distap - distbp;
}
int green(const void *a,const void *b)
{
    double ax2 = ((struct cell *)a) -> x;
    double ay2 = ((struct cell *)a) -> y;
    double distap1 = sqrt((myXp -ax2)*(myXp -ax2) + (myYp-ay2)*(myYp-ay2) );
    
    double bx2 = ((struct cell *)b) -> x;
    double by2 = ((struct cell *)b) -> y;
    double distbp2 = sqrt((myXp -bx2)*(myXp -bx2) + (myYp-by2)*(myYp-by2) );
    
    return distap1 - distbp2;
    
}

struct action playerMove(struct player me, 
                         struct player * players, int nplayers,
                         struct food * foods, int nfood,
                         struct cell * virii, int nvirii,
                         struct cell * mass, int nmass)
{
    struct action act;
    
    myX = me.x;
    myY = me.y;
    
    
    if(start == 0 && start != 10)
    {
        randv = 10;
        act.dx = randv + me.x;
        start++; 
    }
    qsort(foods, nfood, sizeof(struct food), dist);
    qsort(players,nplayers,sizeof(struct player),distplayer);
    qsort(virii,nvirii,sizeof(struct cell),green);

   if( ((abs(virii[0].x - myX) <= 10) || (abs(virii[0].y - myY) <= 10)) )
    {
        virusvar = 1;
        
        if(abs(virii[0].x - myX) <= 10)
        {
            act.dx = (myX - virii[0].x ) * 100;
            act.fire = 0;
            act.split = 0;
            
        }
        if(abs(virii[0].y - myY) <= 10)
        {
            act.dy = (myY - virii[0].y ) * 100;
            act.fire = 0;
            act.split = 0;   
        }
        
    } 
    if( ((abs(virii[0].x - myX) > 10) && (abs(virii[0].y - myY) > 10)) )
    {
        virusvar = 0;
    }  
    
    qsort(foods, nfood, sizeof(struct food), dist);
        qsort(players,nplayers,sizeof(struct player),distplayer);
        qsort(virii,nvirii,sizeof(struct cell),green);
        
    
        if(me.ncells > 1)
        {
            act.fire = 1;
            act.split = 1;
        }
       
        if((((abs(players[0].x - myX) > 200) && (abs(players[0].y - myY) > 200))) && virusvar == 0) 
        {
            act.dx = (foods[0].x - myX) * 1000;
            act.dy = (foods[0].y - myY) * 1000;
            act.fire = 0;
            act.split = 0;
            
        }
        
        
        if( (((abs(players[0].x - myX) <= 200) || (abs(players[0].y - myY) <= 200))) && virusvar == 0 )
        {
            
            if(abs(players[0].x - myX) <= 200)  
            {
                act.dx = (myX - players[0].x ) * 10;
                act.fire = 0;
                act.split = 0;
            }
            if(abs(players[0].y - myY) <= 200)
            {
                act.dy = (myY - players[0].y ) * 11.5;
                act.fire = 0;
                act.split = 0;   
            }
            
        } 
        int i = 2;
        if(act.split==1)
        {
            while(act.split==1)
            {
                act.dx = ((myX+i) * (myX+i))*3.145;
                act.dy = (myY+i);
                i++;
            }
        }
    return act;
}